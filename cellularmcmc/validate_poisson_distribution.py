import collections

import numpy

from cellularmcmc import History, HistoryModel, step_mcmc, exponential_distribution_logpdf

copy_rate_matrix = numpy.array([[0]])

start = numpy.array([0])
history = History(start, [], 20)


model = HistoryModel(
    copy_rate_matrix=copy_rate_matrix,
    mutation_rate=1,
    languages=range(2000),
)

operator_report = collections.defaultdict(collections.Counter)

print("Calculating initial likelihood…")
loglikelihood = model.loglikelihood(history)
stats = []
for i in range(200000):
    if not i % 1000:
        print(loglikelihood, len(history._times))
    stats.append(len(history._times) - 1)
    history, loglikelihood = step_mcmc(
        model, history, loglikelihood, report=operator_report
    )

print(operator_report)
print(exponential_distribution_logpdf.cache_info())
import matplotlib.pyplot as plt
import scipy

plt.scatter(*zip(*collections.Counter(stats).most_common()))
plt.scatter(range(max(stats)), scipy.stats.poisson(20).pmf(range(max(stats)))*len(stats))
plt.show()

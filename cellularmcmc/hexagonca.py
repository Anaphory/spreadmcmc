import string
import scipy
import numpy

""" Hexagonal neighborhoods for cellularmcmc

Hexagonal neighborhoods are represented as two 2D numpy arrays.

The first array describes the cells:

0 1 2 3
 4 5 6 7
8 9 A B
 C D E F

The second array describes the connections:

01 12 23 --
04 15 26 37
-- 14 25 36
45 56 67 --
48 59 6A 7B
-- 49 5A 6B
89 9A AB --
8C 9D AE BF
-- 9C AD BE

So the connections of cell (i, j) are given by

"""

short_ids = string.digits + string.ascii_uppercase


def pretty(matrix, labels, zeros=False):
    """Pretty-print the matrix, using labels for rows and columns.

    >>> pretty([[0, 1], [1, 1]], ["A", "B"])
      A B
    A   1
    B 1 1
    """
    # Limitations:
    #  - Currently only works for square matrices
    #  - Currently assumes all labels have the same lengths
    #  - Currently assumes the values have the same lengths as the labels
    matrix = numpy.asarray(matrix)
    n_labels = max(matrix.shape)
    longest_label = max(len(label) for label in labels[:n_labels])
    print(" " * longest_label, *labels[:n_labels])
    for i in range(n_labels):
        row = [(v if zeros or v else " ") for v in matrix[i]]
        print(labels[i], *row)


def generate_hexagon_copy_rate_matrix(hex_rows, hex_columns):
    """Generate a uniform hexagon copy rate matrix.

    For example, in the case of four “rows” and four “columns”, the grid looks like
     0 1 2 3
      4 5 6 7
     8 9 A B
      C D E F

    so the result of this function is a 16×16 symmetric sparse matrix
    >>> pretty(generate_hexagon_copy_rate_matrix(4, 4).todense(), short_ids)
      0 1 2 3 4 5 6 7 8 9 A B C D E F
    0   1     1
    1 1   1   1 1
    2   1   1   1 1
    3     1       1 1
    4 1 1       1     1 1
    5   1 1   1   1     1 1
    6     1 1   1   1     1 1
    7       1     1         1
    8         1         1     1
    9         1 1     1   1   1 1
    A           1 1     1   1   1 1
    B             1 1     1       1 1
    C                 1 1       1
    D                   1 1   1   1
    E                     1 1   1   1
    F                       1     1
    """
    dim = hex_rows * hex_columns
    matrix = scipy.sparse.dok_array((dim, dim), dtype=int)
    for row in range(0, hex_rows):
        f = -1 if row % 2 == 0 else 1  # column offset in neighboring rows
        for column in range(0, hex_columns):
            if column > 0:
                matrix[row * hex_columns + column, row * hex_columns + column - 1] = 1
                matrix[row * hex_columns + column - 1, row * hex_columns + column] = 1
            if column < hex_columns - 1:
                matrix[row * hex_columns + column, row * hex_columns + column + 1] = 1
                matrix[row * hex_columns + column + 1, row * hex_columns + column] = 1
            if row > 0:
                matrix[row * hex_columns + column, (row - 1) * hex_columns + column] = 1
                matrix[(row - 1) * hex_columns + column, row * hex_columns + column] = 1
                if 0 < column + f < hex_columns - 1:
                    matrix[
                        row * hex_columns + column,
                        (row - 1) * hex_columns + (column + f),
                    ] = 1
                    matrix[
                        (row - 1) * hex_columns + (column + f),
                        row * hex_columns + column,
                    ] = 1
            if row < hex_rows - 1:
                matrix[row * hex_columns + column, (row + 1) * hex_columns + column] = 1
                matrix[(row + 1) * hex_columns + column, row * hex_columns + column] = 1
                if 0 < column + f < hex_columns - 1:
                    matrix[
                        row * hex_columns + column,
                        (row + 1) * hex_columns + (column + f),
                    ] = 1
                    matrix[
                        (row + 1) * hex_columns + (column + f),
                        row * hex_columns + column,
                    ] = 1
    return matrix



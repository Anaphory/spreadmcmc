import collections

import numpy

from cellularmcmc import History, HistoryModel, step_mcmc

copy_rate_matrix=numpy.array([[0]])

start = numpy.array([0])
history = History(start, [], 20)


model = HistoryModel(
    copy_rate_matrix=copy_rate_matrix,
    mutation_rate=1,
    languages=range(2000),
)

operator_report = collections.defaultdict(collections.Counter)

print("Calculating initial likelihood…")
loglikelihood = model.loglikelihood(history)
for i in range(2000):
    if not i%100:
        print(loglikelihood, len(history._times))
        print(history)
    history, loglikelihood = step_mcmc(model, history, loglikelihood, report=operator_report)

print(operator_report)
